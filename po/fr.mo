��          �      ,      �     �     �  ]   �  ,   �     !     8     ?     T     g     n     �     �     �  V   �  q     ~     �  �     �     �  �   �  3   *     ^     w     �     �     �  3   �     �            c   2  �   �  �        
               	                                                          Cancel Close Delete EXIF metadata on JPEG files (often contains shot\ndetails like date, GPS position…). Maximum size of the largest side, in pixels: Processing pictures… Reduce Reduce pictures size Reduce pictures… Rotate Rotate clockwise: Rotate pictures Rotate pictures… Some error occured Source pictures will be preserved and copies will be created with "_rotated" suffix.\n Source pictures will be preserved. Reduced copies will keep source proportions and will take "_reduced" suffix.\n The following files could not be processed: please check that you can read those and that you can create files in this folder. Project-Id-Version: 
Report-Msgid-Bugs-To: yvan.masson@openmailbox.org
POT-Creation-Date: 2017-02-11 20:28+0100
PO-Revision-Date: 2017-02-10 15:56+0100
Last-Translator: Yvan Masson <yvan.masson@openmailbox.org>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n > 1);
 Annuler Fermer Supprimer les méta-données EXIF des fichiers JPEG (qui\nsouvent contiennent des détails sur la photo comme\nla date, la position GPS…). Taille maximale du plus grand côté, en pixels : Traitement des images… Réduire Réduire la taille des images Réduire les images… Tourner Tourner dans le sens des aiguilles d'une montre : Tourner les images Tourner les images… Une erreur est survenue Les images d'origine seront conservées et des copies seront créées avec le suffixe "_rotated".\n Les images d'origine seront conservées. Les copies réduites garderont les proportions d'origine et auront le suffixe "_reduced".\n Les fichiers suivants n'ont pas pu être traité : veuillez vérifier que vous pouvez les lire et que vous pouvez créer des fichiers dans ce répertoire. 